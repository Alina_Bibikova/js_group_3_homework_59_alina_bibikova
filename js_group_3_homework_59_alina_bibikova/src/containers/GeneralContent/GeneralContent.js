import React, {Component, Fragment} from 'react';
import ContentAdd from '../../components/ContentAdd/ContentAdd';
import ContentAll from '../../components/ContentAll/ContentAll';

class GeneralContent extends Component {

    state = {
        movieName: '',
        newItem: [],
    };

    changeInput = event => {
        this.setState({movieName: event.target.value})
    };

    addClick = () => {
        if(this.state.movieName !== '') {
            const items = [...this.state.newItem];
            const newItem = {
                text: this.state.movieName,
            };

            items.push(newItem);

            this.setState({
                newItem: items,
                movieName: ''
            });
        } else {
            alert('Please fill fields!');

        }
    };

    removeClick = (text) => {
        let itemArray = [...this.state.newItem];

        const index = itemArray.findIndex((item) => item.text === text);
        itemArray.splice(index, 1);

        this.setState({newItem: itemArray })
    };


    changeFilmHandler = (id, value) => {
        let newItem = this.state.newItem;
        newItem[id].text = value;
        this.setState({newItem})
    };

    render() {
        return (
            <Fragment>
                <ContentAdd
                    addClick={this.addClick}
                    itemName={this.state.movieName}
                    changed={(event) => this.changeInput(event)}
                >
                    <h2>To watch list:</h2>
                    {this.state.newItem.map((task, key) => {
                        return <ContentAll
                            change={this.changeFilmHandler}
                            key={key}
                            contentAll={task.text}
                            id={key}
                            removeItem={() => this.removeClick(task.text)}
                        />
                    })
                    }
                </ContentAdd>
            </Fragment>
        )
    }
}

export default GeneralContent;
