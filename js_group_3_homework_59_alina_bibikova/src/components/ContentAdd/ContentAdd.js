import React from 'react';
import './ContentAdd.css'

const ContentAdd = props => (
    <div className="ContentAdd">
        <input className='movieName'
               type="movieName"
               value={props.itemName}
               onChange={props.changed}
               placeholder='Movie name'
        />
        <button className='add' onClick={props.addClick}>Add</button>
        <div>
            {props.children}
        </div>
    </div>
);

export default ContentAdd;