import React, {Component} from 'react';
import './ContentAll.css';

class ContentAll extends Component{

    shouldComponentUpdate(nextProps){
        return this.props.contentAll !== nextProps.contentAll;
    }

    render(){
        return (
            <div className='contentAll'>
                <input
                    onChange={(e) => this.props.change(this.props.id, e.target.value)}
                    type="text"
                    value={this.props.contentAll}
                />
                <button className='removeItem' onClick={this.props.removeItem}>x</button>
            </div>
        )
    }

}

export default ContentAll;