import React, { Component } from 'react';
import GeneralContent from './containers/GeneralContent/GeneralContent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
          <GeneralContent/>
      </div>
    );
  }
}

export default App;
