import React, { PureComponent, Fragment } from 'react';
import './Blog.css';

class Blog extends PureComponent {
    state = {
        jokes: "",
    };

    componentDidMount() {
        fetch('https://api.chucknorris.io/jokes/random')
            .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something wrong with network request');
        }).then(posts => {
            this.setState({jokes: posts.value});
        }).catch(error => {
            console.log(error);
        })
    }

    render() {
        return (
            <Fragment>
                <div className='jokes'>
                    <p><strong>Jokes:</strong> {this.state.jokes}</p>
                </div>
            </Fragment>
        );
    }
}

export default Blog;
